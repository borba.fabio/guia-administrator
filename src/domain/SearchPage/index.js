import React from 'react';
import { useHistory } from 'react-router-dom';

import {
  Container,
  Line,
  Box,
  List,
  Header,
  Title,
  Actions,
  Body,
  ListItem,
  Label,
  Adress,
  Icon,
  ToolTip,
  ClientActive,
  PhoneNumber,
} from './styles';
import Root from './../../components/atoms/root';
import InputGuia from './../../components/atoms/input';
import InfoHeader from '../../components/molecules/infoheader';
import ButtonGuia from './../../components/atoms/button';
import CheckBox from '../../components/atoms/checkbox';

const SearchPage = () => {
  const history = useHistory();

  return (
    <Container>
      <Line>
        <Root>
          <InfoHeader
            title="Buscar Contatos"
            subTitle="Realize a busca de clientes em sua base de dados"
          />
        </Root>
      </Line>
      <Root>
        <Box>
          <InputGuia
            login
            placeholder="Pesquisar por nome, telefone ou endereço"
            type="text"
          />
        </Box>
      </Root>
      <Root>
        <List>
          <Header>
            <Title>Listagem de todos os contatos</Title>
            <Actions>
              <ButtonGuia text="Editar" tertiary />
              <ButtonGuia text="Baixar PDF" primary disabled={true} />
              <ButtonGuia
                text="Tornar Cliente"
                primary
                onClick={() => history.push('/contract/upgrade')}
              />
            </Actions>
          </Header>
          <Body>
            <ListItem for="client-fernanda-1">
              <CheckBox
                id="client-fernanda-1"
                name="client-fernanda-1"
                value="Fernanda"
              />
              <Label for="client-fernanda-1">Fernanda</Label>
              <Adress>Rua minas gerais ...</Adress>
              <PhoneNumber>46 000000000</PhoneNumber>
              <ClientActive>
                <Icon />
                <ToolTip>cliente ativo</ToolTip>
              </ClientActive>
            </ListItem>
            <ListItem for="client-fernanda-2">
              <CheckBox
                id="client-fernanda-2"
                name="client-fernanda-2"
                value="Fernanda"
              />
              <Label for="client-fernanda-2">Fernanda 2</Label>
              <Adress>Rua minas gerais ...</Adress>
              <PhoneNumber>46 000000000</PhoneNumber>
              <ClientActive>
                <Icon />
                <ToolTip>cliente ativo</ToolTip>
              </ClientActive>
            </ListItem>
          </Body>
        </List>
      </Root>
    </Container>
  );
};

export default SearchPage;
