import styled from 'styled-components'

import { FiCheck } from 'react-icons/fi'

export const Container = styled.div``

export const Line = styled.div`
border-bottom: 1px solid var(--gray-border)
`
export const Box = styled.div`
padding: 5% 0 1% 0;
max-width: 50%;
`


export const List = styled.div`
border: 1px solid var(--gray-border);
border-radius: 8px;
margin-bottom: 5%;
`
export const Header = styled.div`
display: flex;
flex-direction: row;
flex: 1;
justify-content: space-between;
align-items: center;
padding: 42px;
`
export const Title = styled.div`
font-size: 1.4em;
font-weight: bold;
diplay: flex;
flex: 1;
`
export const Actions = styled.div`
display: flex;
flex-direction: row;
justify-content: space-between;
flex: 1;
`
export const Body = styled.div`
border-top: 1px solid var(--gray-border);

`
export const ListItem = styled.label`
padding: 24px 42px;
display: grid;
grid-template-columns: 0.3fr 1fr 1.5fr 1fr 1fr;
justify-content: center;
align-items: center;
border-top: 1px solid var(--gray-border);
cursor: pointer;
&:hover {
    background-color: var(--tertiary)
} 
`
export const Adress = styled.span`
font-size: 14px;
color: var(--gray-light);
`
export const Icon = styled(FiCheck)`
color: var(--green);
font-size: 24px;
margin-right: 10px;
`
export const ToolTip = styled.div`
background: var(--gray-light);
color: white;
padding: 5px 10px;
border-radius: 4px;
position: relative;
&:before {
    content: "";
    position: absolute;
    top: 4px;
    left: -8px;
    border-style: solid;
    border-width: 10px 12px 10px 0;
    border-color: transparent var(--gray-light) transparent transparent;
    display: block;
    width: 0;
    z-index: 0;
    }
`

export const ClientActive = styled.div`
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;

`
export const PhoneNumber = styled.span`
font-size: 22px;
font-weight: bold;
color: var(--gray-light);
`
export const Label = styled.h1`
font-size: 1.2em;
font-weight: bold;
color: var(--gray-light);
`
