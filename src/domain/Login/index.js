import React from 'react';
import { useHistory } from 'react-router-dom';

import {
  Container,
  Header,
  Box,
  Logo,
  Actions,
  Action,
  Form,
} from './styles';
import logoSource from './../../assets/images/guia-logotipo.svg';
import ButtonGuia from './../../components/atoms/button';
import InputGuia from './../../components/atoms/input';

const Login = () => {
  const history = useHistory();

  return (
    <>
      <Header>
        <ButtonGuia text="Voltar" tertiary />
      </Header>
      <Container>
        <Box>
          <Logo src={logoSource} />
          <Form>
            <InputGuia type="email" placeholder="@g123.com.br" />
            <InputGuia type="password" placeholder="Senha" />
            <Action>
              <ButtonGuia
                text="Acessar minha conta"
                secondary
                large
                onClick={() => history.push('/search-page')}
              />
            </Action>
          </Form>
          <Actions>
            <ButtonGuia text="Cadastrar meu número" primary large />
          </Actions>
        </Box>
      </Container>
    </>
  );
};

export default Login;
