import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  margin: 3% 0;
`;
export const Header = styled.div`
  position: absolute;
  left: 43px;
  top: 34px;
`;

export const Box = styled.div`
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.3);
  border: 1px solid var(--gray-border);
  border-radius: 8px;
  flex-direction: column;
  background: white;
  flex: 1 1 0%;
  display: flex;
  -webkit-box-pack: center;
  justify-content: center;
  -webkit-box-align: center;
  align-items: center;
  min-width: 446px;
  padding: 2% 0px;
`;

export const Form = styled.div`
  display: flex;
  flex: 1;
  width: 100%;
  flex-direction: column;
  padding: 0 34px;
`;
export const Logo = styled.img`
  padding-bottom: 37px;
  display: flex;
  flex: 1;
  width: 200px;
`;
export const Actions = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  width: 100%;
  padding: 0 34px;
  margin-top: 50px;
`;
export const Action = styled.div`
  margin-top: 10px;
  display: flex;
  flex: 1;
  flex-direction: column;
  width: 100%;
`;
