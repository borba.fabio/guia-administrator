import styled from 'styled-components'
import { NavLink } from 'react-router-dom'

export const Container = styled.div`
  display: flex;
  padding: 0;
  padding-bottom: 16px;
`

export const MenuLink = styled(NavLink)`
  color: var(--gray-base);
  font-size: 15px;
  display: flex;
  text-decoration: none;
  text-align: center;
  padding 0;
  padding-bottom: 12px;
  padding-right: 8px;
  padding-left: 8px;
  margin-right: 1.2em;
  transition: color .2s ease;
  outline: none;
  cursor: pointer;

  &.active {
    color: var(--secondary);
    border-bottom: 2px solid var(--secondary);
  }
`
export const Title = styled.span``
