import React from 'react';
import { useHistory } from 'react-router-dom';

import LogoImage from './../../../assets/images/guia-logotipo.svg';

import { Container, Image, Profile, ProfileActions } from './styles';
import GuiaButton from './../../atoms/button';

const Login = () => {
  const history = useHistory();

  return (
    <Container>
      <Image src={LogoImage} />
      <Profile>
        <ProfileActions>
          <GuiaButton primary link text="Olá Cleomar" />
          <GuiaButton
            link
            text="Sair"
            onClick={() => history.push('/')}
          />
        </ProfileActions>
      </Profile>
    </Container>
  );
};

export default Login;
