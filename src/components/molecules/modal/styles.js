import styled from 'styled-components'

import { FiX } from 'react-icons/fi'

import Modal from 'react-modal';

export const Container = styled(Modal)`
height: 100vh;
background: rgba(0,0,0,0.3);
padding-top: 60px;
`
export const CloseIcon = styled(FiX)`
font-size: 22px;
display: flex;
justify-content: center;
align-items: center;
`

export const Box = styled.div`
background: white;
padding: 40px;
margin: 0 auto;
width: 1024px;
border-radius: 8px;
`;


export const CloseButton = styled.div`
background: white;
border: 1px solid var(--gray-base);
border-radius: 8px;
display: inline-block;
position: absolute;
right: 0;
top: 0;
cursor pointer;
padding: 8px;
`
export const Header = styled.div`
background: red;
display: flex;
flex: 1;
align-items: flex-end;
position: relative;
`
export const Content = styled.div`
align-items: center;
flex: 1;
justify-content: center;
display: flex;
flex-direction: column;
margin-top: 110px;
`
export const Image = styled.img`
  height: 80px;
  margin-bottom: 20px;
`
export const Title = styled.div`
display: flex;
flex: 1;
flex-direction: column;
align-items: center;
color: rgba(0,0,0, 1);
font-size: 1.4em;
font-weight: bold;
padding: 10px 0;
`
export const SubTitle = styled.div``
export const Actions = styled.div`
display: flex;
flex: 1;
flex-direction: row;
justify-content: space-between;
margin: 12% 0 0 0;
`
export const Default = styled.div`
display: flex;
`
export const Custom = styled.div`
display: flex;

button:first-child {
  margin-right: 20px;
}
`