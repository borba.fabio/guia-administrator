import styled from 'styled-components'

export const Container = styled.section`
  padding: 2.2em 0;
`
export const Title = styled.h1`
  padding-bottom: 10px;
  color: var(--gray-base)
`
export const SubTitle = styled.span`
  color: var(--gray-base);
  font-size: 16px;
`