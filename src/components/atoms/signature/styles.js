import styled from 'styled-components'

export const Container = styled.div`

  .sigCanvas {
    border: 1px solid var(--gray-border);
    border-radius: 8px;
  }
`