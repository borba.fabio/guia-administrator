import React from 'react'

import { Input } from './styles'

const CheckBox = ({ id, value, name, checked, label }) => (
    <Input type="checkbox" id={id} name={name} value={value} checked={checked} />
)

export default CheckBox
