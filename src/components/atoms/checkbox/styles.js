import styled from 'styled-components'


export const Input = styled.input`
width: 16px;
height: 16px;
`
export const Label = styled.label`
font-size: 14px;
color: var(--gray-light);
padding: 10px 2px;
display: block;
`