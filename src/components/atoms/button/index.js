import React from 'react';
import { Button, IconBox, Loader } from './styles';

const ButtonGuia = ({
  text,
  primary,
  secondary,
  tertiary,
  link,
  large,
  loading,
  iconRight,
  iconLeft,
  onClick,
  disabled,
}) => {
  return (
    <Button
      primary={primary}
      secondary={secondary}
      tertiary={tertiary}
      link={link}
      large={large}
      loading={loading}
      onClick={onClick}
      disabled={disabled}
    >
      {loading ? (
        <IconBox>
          <Loader size="24" />
        </IconBox>
      ) : (
        <>
          {iconLeft && <IconBox>{iconLeft}</IconBox>}
          {text}
          {iconRight && <IconBox>{iconRight}</IconBox>}
        </>
      )}
    </Button>
  );
};

export default ButtonGuia;
