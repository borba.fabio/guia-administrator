import styled from 'styled-components'

export const Container = styled.div`
display: flex;
flex-direction: row;
flex: 1;
align-items: center;
`
export const Input = styled.input`

`
export const Label = styled.label`
font-size: 14px;
color: var(--gray-light);
padding: 10px 2px;
display: block;
`