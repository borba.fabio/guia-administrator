import React from 'react'

import { Container, Title, SubTitle } from './styles'

const FormDescription = ({ title, subtile }) => (
  <Container>
    <Title>{title}</Title>
    <SubTitle>{subtile}</SubTitle>
  </Container>
)

export default FormDescription;