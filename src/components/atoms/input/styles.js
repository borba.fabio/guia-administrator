import styled, { css } from 'styled-components'


export const Container = styled.div`
display: flex;
flex-direction: column;
flex: 1;
margin-right: 20px;
`
export const Input = styled.input`
flex: 1;
display: inline-block;
border-radius: 8px;
padding: 14px 24px;
font-size: 14px;
outline: none;
border: 1px solid var(--gray-border);
transition: all 500ms ease 0s;
margin-bottom: 28px;


&:hover {
  border: 1px solid var(--secondary);
}
${(props) =>
    props.login &&
    css`
  box-shadow: 0 3px 6px rgba(0,0,0, 0.3)
  `}


  ${(props) =>
    props.error &&
    css`
    border: 1px solid red !important;
  `}
  
  `
export const Label = styled.span`
  font-size: 14px;
  color: var(--gray-light);
  padding: 10px 2px;
  display: block;
`
export const TextArea = styled.textarea`
  flex: 1;
display: inline-block;
border-radius: 8px;
padding: 14px 24px;
font-size: 14px;
outline: none;
border: 1px solid var(--gray-border);
transition: all 500ms ease 0s;
margin-bottom: 28px;
resize:vertical;
`