import React from 'react'
import { Switch } from 'react-router-dom'

import Auth from './auth'
import Route from './routes'

import Login from '../domain/Login'
import SearchPage from '../domain/SearchPage'
import CreateContact from '../domain/Contract/Create'
import UpgradeContract from '../domain/Contract/Upgrade'

const Routes = () => (
  <Switch>
    <Route path="/" exact isPublic component={Login} />
    <Route path="/search-page" component={SearchPage} />
    <Route path="/contract/create" component={CreateContact} />
    <Route path="/contract/upgrade" component={UpgradeContract} />
  </Switch>
)

export default Auth()(Routes)