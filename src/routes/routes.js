import React from 'react'
import { Route } from 'react-router-dom'

import DefaultLayout from './../layout/default'
import AuthLayout from './../layout/login'

export default function RouteWrapper({
  component: Component,
  isPublic,
  path,
  ...rest
}) {
  const Layout = isPublic ? AuthLayout : DefaultLayout
  return (
    <Route
      {...rest}
      path={path}
      isPublic={isPublic}
      render={(props) => (
        <Layout>
          <Component {...props} />
        </Layout>
      )}
    />
  )
}

RouteWrapper.defaultProps = {
  isPublic: false,
  exact: true,
  path: null,
}
