import React, { memo } from 'react'

const Auth = () => (Component) => {
  const Auth = (props) => {
    return <Component {...props} />
  }

  return memo(Auth)
}

export default Auth
